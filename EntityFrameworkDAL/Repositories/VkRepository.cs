﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities;
using Domain.Repositories;
using EntityFrameworkDAL.Context;

namespace EntityFrameworkDAL.Repositories
{
  public class VkRepository : IVkRepository, IDisposable
  {
    private bool _isDisposed;
    private readonly VkContext _vkContext;

    public VkRepository()
    {
      _vkContext = new VkContext();
    }

    public bool IsDisposed
    {
      get { return _isDisposed; }
    }

    public PlayList AddPlayList(PlayList playList)
    {
      var userPlayLists = GetUserPlayLists(playList.UserId);

      if (userPlayLists.FirstOrDefault(r => r.Name == playList.Name) != null)
      {
        throw new Exception("Playlist with " + playList.Name + " name already exists");
      }

      _vkContext.PlayLists.Add(playList);
      _vkContext.SaveChanges();

      return playList;
    }

    public PlayList AddTrackToPlaylist(int playListId, AudioTrack audioTrack)
    {
      var playList = GetPlayListById(playListId);

      if (playList == null)
      {
        throw new Exception("Playlist doesn't exist");
      }

      playList.AudioTracks.Add(audioTrack);
      _vkContext.SaveChanges();

      return playList;
    }

    public PlayList GetPlayListById(int id)
    {
      return _vkContext.PlayLists.FirstOrDefault(r => r.Id == id);
    }

    public IEnumerable<PlayList> GetUserPlayLists(int userId)
    {
      var playLists = _vkContext.PlayLists.Where(r => r.UserId == userId).ToList();

      foreach (PlayList playList in playLists)
      {
        playList.AudioTracks = playList.AudioTracks.OrderBy(r => r.Id).ToList();
      }

      return playLists;
    }

    public void DeletePlaylist(int id)
    {
      var playlist = _vkContext.PlayLists.FirstOrDefault(r => r.Id == id);

      if (playlist == null)
      {
        return;
      }

      _vkContext.PlayLists.Remove(playlist);
      _vkContext.SaveChanges();
    }

    public void DeleteAudioTrack(int id)
    {
      var track = _vkContext.AudioTracks.FirstOrDefault(r => r.Id == id);

      if (track == null)
      {
        return;
      }

      _vkContext.AudioTracks.Remove(track);
      _vkContext.SaveChanges();
    }

    public PlayList EditPlayList(int id, string name, string imageUrl)
    {
      var playList = _vkContext.PlayLists.FirstOrDefault(r => r.Id == id);

      if (playList == null)
      {
        throw new Exception("Playlist not found");
      }

      playList.Name = name;
      playList.Image = imageUrl;

      _vkContext.SaveChanges();
      return playList;
    }

    public void UpdateTrack(AudioTrack audioTrack) {
      var track = _vkContext.AudioTracks.FirstOrDefault(r => r.Id == audioTrack.Id);

      if(track == null)
      {
        return;
      }

      track.Artist = audioTrack.Artist;
      track.Title = audioTrack.Title;
      track.VkTrackId = audioTrack.VkTrackId;
      track.PlayList = audioTrack.PlayList;
      track.OwnerId = audioTrack.OwnerId;
      track.Url = audioTrack.Url;

      _vkContext.SaveChanges();
    }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    public virtual void Dispose(bool disposing)
    {
      if (_isDisposed == false)
      {
        if (disposing)
        {
          _vkContext.Dispose();
        }
      }

      _isDisposed = true;
    }

    public IList<PlayList> GetAllPlayLists()
    {
      return _vkContext.PlayLists.ToList();
    }
  }
}
