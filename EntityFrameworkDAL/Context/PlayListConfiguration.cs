﻿using System.Data.Entity.ModelConfiguration;
using Domain.Entities;

namespace EntityFrameworkDAL.Context
{
  public class PlayListConfiguration : EntityTypeConfiguration<PlayList>
  {
    public PlayListConfiguration()
    {
      HasMany(r => r.AudioTracks).WithRequired(r => r.PlayList).WillCascadeOnDelete(true);
    }
  }
}
