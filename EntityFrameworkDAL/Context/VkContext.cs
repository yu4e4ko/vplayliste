﻿using System.Data.Entity;
using Domain.Entities;

namespace EntityFrameworkDAL.Context
{
  public class VkContext : DbContext
  {
    public VkContext(): base("name=VkContext")
    {
      Database.SetInitializer<VkContext>(new DropCreateDatabaseIfModelChanges<VkContext>());
    }

    public DbSet<AudioTrack> AudioTracks { get; set; }

    public DbSet<PlayList> PlayLists { get; set; }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
      modelBuilder.Configurations.Add(new PlayListConfiguration());
    }
  }
}
