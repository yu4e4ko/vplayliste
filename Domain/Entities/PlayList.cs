﻿using System.Collections.Generic;

namespace Domain.Entities
{
  public class PlayList
  {

    public int Id { get; set; }

    public int UserId { get; set; }

    public string Name { get; set; }

    public string Image { get; set; }

    public virtual ICollection<AudioTrack> AudioTracks { get; set; }
  }
}
