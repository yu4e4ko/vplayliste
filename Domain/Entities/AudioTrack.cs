﻿using Newtonsoft.Json;

namespace Domain.Entities
{
  public class AudioTrack
  {
    public int Id { get; set; }

    [JsonIgnore]
    public PlayList PlayList { get; set; }

    public int VkTrackId { get; set; }

    public string Artist { get; set; }

    public string Title { get; set; }

    public int OwnerId { get; set; }

    public string Url { get; set; }
  }
}
