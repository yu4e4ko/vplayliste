﻿using System.Collections.Generic;
using Domain.Entities;

namespace Domain.Repositories
{
  public interface IVkRepository
  {
    PlayList AddPlayList(PlayList playList);

    PlayList AddTrackToPlaylist(int playListId, AudioTrack audioTrack);

    IEnumerable<PlayList> GetUserPlayLists(int userId);

    IList<PlayList> GetAllPlayLists();

    PlayList GetPlayListById(int id);

    void DeletePlaylist(int id);

    void DeleteAudioTrack(int id);

    PlayList EditPlayList(int id, string name, string imageUrl);

    void UpdateTrack(AudioTrack audioTrack);
  }
}
