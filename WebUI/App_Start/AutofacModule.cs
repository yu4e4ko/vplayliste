﻿using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Domain.Repositories;
using EntityFrameworkDAL.Repositories;

namespace WebUI.App_Start
{
  internal class AutofacModule
  {
    public static void RegisterAutoFac()
    {
      var builder = new ContainerBuilder();
      builder.RegisterControllers(Assembly.GetExecutingAssembly());
      builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

      builder.RegisterType<VkRepository>().As<IVkRepository>()
          .InstancePerRequest();

      var container = builder.Build();
      DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

      var config = GlobalConfiguration.Configuration;
      var resolver = new AutofacWebApiDependencyResolver(container);
      config.DependencyResolver = resolver;

    }
  }
}
