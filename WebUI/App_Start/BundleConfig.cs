﻿using System.Web;
using System.Web.Optimization;

namespace WebUI
{
  public class BundleConfig
  {
    // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
    public static void RegisterBundles(BundleCollection bundles)
    {
      bundles.Add(new ScriptBundle("~/bundles/jquery").Include(            
                  "~/Scripts/jquery-{version}.js",
                  "~/Scripts/jquery-ui-1.11.4.js"));

      bundles.Add(new ScriptBundle("~/bundles/jplayer").Include(
                  "~/Scripts/jPlayer.2.6.0/jquery.jplayer.min.js",
                  "~/Scripts/jPlayer.2.6.0/jplayer.playlist.min.js"));

      // Use the development version of Modernizr to develop with and learn from. Then, when you're
      // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
      bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                  "~/Scripts/modernizr-*"));

      bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/respond.js"));

      bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/audioPlayer.css",
                "~/Content/vkontakte/vkontakte.css",
                "~/Content/bootstrap-slate.css",
                "~/Content/bootstrap-responsive.css",
                "~/Content/dialog-master.css",
                "~/Content/site.css"));

      bundles.Add(new ScriptBundle("~/bundles/ko").Include(
                "~/Scripts/knockout-3.3.0.js",
                "~/Scripts/knockout.mapping-latest.js",
                "~/Scripts/knockout.validation.min.js"));

      bundles.Add(new ScriptBundle("~/bundles/pagerjs").Include(
                 "~/Scripts/pager.js"));

      bundles.Add(new ScriptBundle("~/bundles/mousetrap").Include(
                 "~/Scripts/mousetrap.min.js"));

      #region Application JS
      //--------------------------  KO  --------------------------------
      bundles.Add(new ScriptBundle("~/bundles/initSiteJS").Include(
                  "~/Scripts/InitSiteJS/init.js"));

      bundles.Add(new ScriptBundle("~/bundles/common").Include(
                  "~/Scripts/Common/errorDialog.js",
                  "~/Scripts/Common/ajaxLoadingPopup.js",
                  "~/Scripts/Common/vplaylisteCore.js"));

      bundles.Add(new ScriptBundle("~/bundles/vk").Include(
                  "~/Scripts/Vk/vkAudio.js",
                  "~/Scripts/Vk/vkPlaylists.js",
                  "~/Scripts/Vk/vkSharing.js",
                  "~/Scripts/Vk/vkUser.js"));

      bundles.Add(new ScriptBundle("~/bundles/jPlayerScripts").Include(
                  "~/Scripts/AudioPlayer/jPlayer.js"));

      //--------------------------  KO  --------------------------------
      #endregion


      // Set EnableOptimizations to false for debugging. For more information,
      // visit http://go.microsoft.com/fwlink/?LinkId=301862
      BundleTable.EnableOptimizations = true;
    }
  }
}
