﻿using System;
using System.Linq;
using System.Web.Http;
using Domain.Entities;
using Domain.Repositories;
using WebUI.Models;

namespace WebUI.Controllers
{
  public class PlayListController : ApiController
  {
    private readonly IVkRepository _vkRepository;

    public PlayListController(IVkRepository vkRepository)
    {
      _vkRepository = vkRepository;
    }

    public IHttpActionResult GetPlayLists(int userId)
    {
      try
      {
        var playLists = _vkRepository.GetUserPlayLists(userId).ToList();

        return Json(new { PlayLists = playLists });
      }
      catch (Exception ex)
      {
        return Json(new {Error = ex.Message});
      }
    }

    [HttpPost]
    public IHttpActionResult AddPlayList([FromBody] AddPlayListModel addPlayListModel)
    {
      try
      {
        var playlistsLimit = WebApiApplication.MaxPlaylistsCount;
        var userPlaylists = _vkRepository.GetUserPlayLists(addPlayListModel.UserId);

        if (userPlaylists != null && userPlaylists.Count() >= playlistsLimit)
        {
          throw new Exception(string.Format("Невозможно создать плейлист. Лимит: {0} плейлистов!", playlistsLimit));
        }

        var playList = new PlayList()
        {
          UserId = addPlayListModel.UserId,
          Name = addPlayListModel.PlayListName,
          Image = addPlayListModel.ImageUrl,
          AudioTracks = addPlayListModel.AudioTracks
        };

        var result = _vkRepository.AddPlayList(playList);

        return Json(result);
      }
      catch (Exception ex)
      {
        return Json(new { Error = ex.Message });
      }
    }
  }
}
