﻿using System;
using System.Web.Http;
using Domain.Repositories;
using WebUI.Models;

namespace WebUI.Controllers
{
  public class EditPlayListController : ApiController
  {
    private readonly IVkRepository _vkRepository;

    public EditPlayListController(IVkRepository vkRepository)
    {
      _vkRepository = vkRepository;
    }

    [HttpPost]
    public IHttpActionResult EditPlayList([FromBody] EditPlayListModel editPlayListModel)
    {
      try
      {
        var result = _vkRepository
          .EditPlayList(editPlayListModel.PlaylistId, editPlayListModel.PlayListName, editPlayListModel.ImageUrl);

        return Json(result);
      }
      catch (Exception ex)
      {
        return Json(new { Error = ex.Message });
      }
    }
  }
}
