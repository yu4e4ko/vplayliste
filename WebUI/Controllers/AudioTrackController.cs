﻿using System;
using System.Web.Http;
using Domain.Entities;
using Domain.Repositories;
using WebUI.Models;

namespace WebUI.Controllers
{
  public class AudioTrackController : ApiController
  {
    private readonly IVkRepository _vkRepository;

    public AudioTrackController(IVkRepository vkRepository)
    {
      _vkRepository = vkRepository;
    }

    [HttpPost]
    public IHttpActionResult AddTrackToPlayList([FromBody] AddTrackToPlayListModel addTrackToPlayListModel)
    {
      try
      {
        var tracksLimit = WebApiApplication.MaxTracksCount;

        var playlist = _vkRepository.GetPlayListById(addTrackToPlayListModel.PlayListId);
        if (playlist != null && playlist.AudioTracks.Count >= tracksLimit)
        {
          throw new Exception(string.Format("Невозможно добавать трек. Лимит: {0} треков в альбоме!", tracksLimit));
        }

        var track = new AudioTrack
        {
          Artist = addTrackToPlayListModel.Artist,
          Title = addTrackToPlayListModel.Title,
          Url = addTrackToPlayListModel.Url,
          VkTrackId = addTrackToPlayListModel.VkTrackId,
          OwnerId = addTrackToPlayListModel.OwnerId
        };

        var playList = _vkRepository.AddTrackToPlaylist(addTrackToPlayListModel.PlayListId, track);

        return Json(playList);
      }
      catch (Exception ex)
      {
        return Json(new {Error = ex.Message});
      }
    }
  }
}
