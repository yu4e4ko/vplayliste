﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Domain.Repositories;
using WebUI.Models;

namespace WebUI.Controllers
{
  public class InfoController : Controller
  {
    private readonly IVkRepository _vkRepository;

    public InfoController(IVkRepository vkRepository)
    {
      _vkRepository = vkRepository;
    }

    public ActionResult Index()
    {
      var playlists = _vkRepository.GetAllPlayLists();

      var infoModels = new List<InfoModel>();

      var users = playlists.GroupBy(r => r.UserId).Select(r => r.ToList()).ToList();

      foreach (var userPlaylists in users)
      {
        var infoModel = new InfoModel
        {
          UserId = userPlaylists.First().UserId
        };

        var playlistCount = userPlaylists.Count();
        var trackCount = userPlaylists.Sum(r => r.AudioTracks.Count);

        infoModel.PlaylistCount = playlistCount;
        infoModel.TrackCount = trackCount;
        infoModel.Playlists.AddRange(userPlaylists.Select(r => r.Id));

        infoModels.Add(infoModel);
      }

      infoModels = infoModels.OrderByDescending(r => r.TrackCount).Take(500).ToList();

      return View(infoModels);
    }
  }
}