﻿using System;
using System.Web.Http;
using Domain.Repositories;
using WebUI.Models;

namespace WebUI.Controllers
{
  public class DeleteAudioTrackController : ApiController
  {
    private readonly IVkRepository _vkRepository;

    public DeleteAudioTrackController(IVkRepository vkRepository)
    {
      _vkRepository = vkRepository;
    }

    [HttpPost]
    public IHttpActionResult Index([FromBody] IdModel idModel)
    {
      try
      {
        if (idModel.Id == 0)
        {
          throw new Exception("Удаление трека: неверный id");
        }

        _vkRepository.DeleteAudioTrack(idModel.Id);

        return Json(new { Success = true });
      }
      catch (Exception ex)
      {
        return Json(new { Error = ex.Message });
      }
    }
  }
}
