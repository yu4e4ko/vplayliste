﻿using System;
using System.Web.Http;
using Domain.Repositories;
using WebUI.Models;

namespace WebUI.Controllers
{
  public class DeletePlayListController : ApiController
  {
    private readonly IVkRepository _vkRepository;

    public DeletePlayListController(IVkRepository vkRepository)
    {
      _vkRepository = vkRepository;
    }

    [HttpPost]
    public IHttpActionResult Index([FromBody] IdModel idModel)
    {
      try
      {
        if (idModel.Id == 0)
        {
          throw new Exception("Удаление плейлиста: неверный id");
        }

        _vkRepository.DeletePlaylist(idModel.Id);

        return Json(new { Success = true });
      }
      catch (Exception ex)
      {
        return Json(new { Error = ex.Message });
      }
    }
  }
}
