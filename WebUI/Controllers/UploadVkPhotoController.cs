﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Http;
using WebUI.Models;

namespace WebUI.Controllers
{
  public class UploadVkPhotoController : ApiController
  {
    [HttpPost]
    public IHttpActionResult UploadImage([FromBody] UploadVkImageModel model)
    {
      try {
        var webClient = new WebClient();
        var pic = webClient.DownloadData(model.ImagePath);

        HttpWebRequest Request = (HttpWebRequest)WebRequest.Create(model.UploadUrl);
        Stream _stream;
        string _boundary = "";// String.Format("--{0}", GetMD5());
        string _templateFile = "--{0}\r\nContent-Disposition: form-data; name=\"{1}\"; filename=\"{2}\"\r\nContent-Type: {3}\r\n\r\n";
        string _templateEnd = "--{0}--\r\n\r\n";
        Request.Method = "POST";
        Request.ContentType = String.Format("multipart/form-data; boundary={0}", _boundary);
        _stream = Request.GetRequestStream();
        string FilePath = model.ImagePath;
        string FileType = "application/octet-stream";
        string Name = "file1";
        byte[] contentFile = Encoding.UTF8.GetBytes(String.Format(_templateFile, _boundary, Name, FilePath, FileType));
        _stream.Write(contentFile, 0, contentFile.Length);
        _stream.Write(pic, 0, pic.Length);
        byte[] _lineFeed = Encoding.UTF8.GetBytes("\r\n");
        _stream.Write(_lineFeed, 0, _lineFeed.Length);
        byte[] contentEnd = Encoding.UTF8.GetBytes(String.Format(_templateEnd, _boundary));
        _stream.Write(contentEnd, 0, contentEnd.Length);
        HttpWebResponse webResponse = (HttpWebResponse)Request.GetResponse();
        StreamReader read = new StreamReader(webResponse.GetResponseStream());


        Char[] read12 = new Char[256];
        int count = read.Read(read12, 0, 256);
        string output = String.Empty;
        while (count > 0)
        {
          String str = new String(read12, 0, count);
          output += str;
          count = read.Read(read12, 0, 256);
        }

        var jsreader = new JsonTextReader(new StringReader(output));
        var json = new JsonSerializer().Deserialize(jsreader);

        return Json(json);
      }
      catch (Exception ex)
      {
        return Json(new { Error = "VK upload image error! " + ex.Message });
      }
    }
  }
}
