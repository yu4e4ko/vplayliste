﻿using Domain.Entities;
using Domain.Repositories;
using System;
using System.Web.Http;
using WebUI.Models;

namespace WebUI.Controllers
{
  public class UpdateTrackController : ApiController
  {
    private readonly IVkRepository _vkRepository;

    public UpdateTrackController(IVkRepository vkRepository)
    {
      _vkRepository = vkRepository;
    }

    [HttpPost]
    public IHttpActionResult Update([FromBody] UpdateTrackModel updateTrackModel)
    {
      try
      {
        var audioTrack = new AudioTrack
        {
          Id = updateTrackModel.TrackId,
          Url = updateTrackModel.Url,
          Artist = updateTrackModel.Artist,
          OwnerId = updateTrackModel.OwnerId,
          Title = updateTrackModel.Title,
          VkTrackId = updateTrackModel.VkTrackId
        };

        _vkRepository.UpdateTrack(audioTrack);

        return Json(new { Success = true });
      }
      catch (Exception ex)
      {
        return Json(new { Error = ex.Message });
      }
    }
  }
}
