﻿using System.Web.Mvc;

namespace WebUI.Controllers
{
  public class VkMusicController : Controller
  {
    public ActionResult Index()
    {
      @ViewBag.sharedId = Session["sharedId"];
      Session["sharedId"] = 0;

      ViewBag.maxTracksCount = WebApiApplication.MaxTracksCount;
      ViewBag.maxPlaylistsCount = WebApiApplication.MaxPlaylistsCount; 
      return View();
    }

    public ActionResult Shared(int? id)
    {     
      Session["sharedId"] = id;
      return RedirectToAction("Index");
    }
  }
}