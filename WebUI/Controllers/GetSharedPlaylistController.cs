﻿using System;
using System.Web.Http;
using Domain.Repositories;

namespace WebUI.Controllers
{
  public class GetSharedPlaylistController : ApiController
  {
    private readonly IVkRepository _vkRepository;

    public GetSharedPlaylistController(IVkRepository vkRepository)
    {
      _vkRepository = vkRepository;
    }

    public IHttpActionResult GetSharedPlayList(int id)
    {
      try
      {
        var playList = _vkRepository.GetPlayListById(id);

        return Json(new { PlayList = playList });
      }
      catch (Exception ex)
      {
        return Json(new { Error = ex.Message });
      }
    }
  }
}
