﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebUI.App_Start;

namespace WebUI
{
  public class WebApiApplication : System.Web.HttpApplication
  {
    public const int MaxPlaylistsCount = 100;
    public const int MaxTracksCount = 500;

    protected void Application_Start()
    {
      AreaRegistration.RegisterAllAreas();

      AutofacModule.RegisterAutoFac();

      GlobalConfiguration.Configure(WebApiConfig.Register);
      FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
      RouteConfig.RegisterRoutes(RouteTable.Routes);
      BundleConfig.RegisterBundles(BundleTable.Bundles);
    }
  }
}
