﻿using System;
using System.Linq.Expressions;
using System.Web.Http.Controllers;
using System.Web.Mvc;
using WebUI.Controllers;

namespace WebUI.Helper
{
  public class NameHelper
  {
    public static class GetSharedPlaylistNameHelper
    {
      public static string Area
      {
        get { return string.Empty; }
      }

      public static string Controller
      {
        get { return GetApiController<GetSharedPlaylistController>(); }
      }
    }

    public static class PlayListNameHelper
    {
      public static string Area
      {
        get { return string.Empty; }
      }

      public static string Controller
      {
        get { return GetApiController<PlayListController>(); }
      }
    }

    public static class AudioTrackNameHelper
    {
      public static string Area
      {
        get { return string.Empty; }
      }

      public static string Controller
      {
        get { return GetApiController<AudioTrackController>(); }
      }
    }

    public static class UploadVkPhotoNameHelper
    {
      public static string Area
      {
        get { return string.Empty; }
      }

      public static string Controller
      {
        get { return GetApiController<UploadVkPhotoController>(); }
      }
    }

    public static class UpdateTrackNameHelper
    {
      public static string Area
      {
        get { return string.Empty; }
      }

      public static string Controller
      {
        get { return GetApiController<UpdateTrackController>(); }
      }
    }

    public static class DeletePlayListNameHelper
    {
      public static string Area
      {
        get { return string.Empty; }
      }

      public static string Controller
      {
        get { return GetApiController<DeletePlayListController>(); }
      }
    }

    public static class DeleteAudioTrackNameHelper
    {
      public static string Area
      {
        get { return string.Empty; }
      }

      public static string Controller
      {
        get { return GetApiController<DeleteAudioTrackController>(); }
      }
    }

    public static class EditPlayListNameHelper
    {
      public static string Area
      {
        get { return string.Empty; }
      }

      public static string Controller
      {
        get { return GetApiController<EditPlayListController>(); }
      }
    }


    #region Private Methods

    private static string GetAction<TController>(Expression<Antlr.Runtime.Misc.Func<TController, object>> action)
      where TController : Controller
    {
      if (action == null)
      {
        throw new ArgumentNullException("action");
      }

      var methodExpression = action.Body as MethodCallExpression;

      if (methodExpression == null)
      {
        throw new ArgumentException("method expression");
      }

      return methodExpression.Method.Name;
    }

    private static string GetController<TController>() where TController : IController
    {
      return GetController(typeof(TController));
    }

    private static string GetApiController<TApiController>() where TApiController : IHttpController
    {
      return GetController(typeof(TApiController));
    }

    public static string GetController(Type controller)
    {
      return controller.Name.Replace("Controller", string.Empty);
    }

    #endregion
  }
}
