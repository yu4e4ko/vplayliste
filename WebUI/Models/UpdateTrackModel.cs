﻿namespace WebUI.Models
{
  public class UpdateTrackModel : AddTrackToPlayListModel
  {
    public int TrackId { get; set; }
  }
}
