﻿namespace WebUI.Models
{
  public class EditPlayListModel
  {
    public int PlaylistId { get; set; }

    public string PlayListName { get; set; }

    public string ImageUrl { get; set; }
  }
}
