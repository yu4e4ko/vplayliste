﻿namespace WebUI.Models
{
  public class VkAuth
  {
    public string Code { get; set; }

    public string AccessToken { get; set; }

    public int UserId { get; set; }

    public string Errors { get; set; }
  }
}
