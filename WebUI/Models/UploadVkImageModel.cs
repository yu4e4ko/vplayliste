﻿namespace WebUI.Models
{
  public class UploadVkImageModel
  {
    public string Action { get; set; }

    public string UploadUrl { get; set; }

    public string ImagePath { get; set; }

    public string User { get; set; }
  }
}