﻿namespace WebUI.Models
{
  public class AddTrackToPlayListModel
  {
    public int PlayListId { get; set; }

    public int VkTrackId { get; set; }

    public string Artist { get; set; }

    public string Title { get; set; }

    public string Url { get; set; }

    public int OwnerId { get; set; }
  }
}
