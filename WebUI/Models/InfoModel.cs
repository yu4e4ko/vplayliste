﻿using System.Collections.Generic;

namespace WebUI.Models
{
  public sealed class InfoModel
  {
    public InfoModel()
    {
      this.Playlists = new List<int>();
    }
    public int UserId { get; set; }

    public int PlaylistCount { get; set; }

    public int TrackCount { get; set; }

    public List<int> Playlists { get; set; }
  }
}
