﻿using System.Collections.Generic;
using Domain.Entities;

namespace WebUI.Models
{
  public class AddPlayListModel
  {
    public AddPlayListModel()
    {
      this.AudioTracks = new List<AudioTrack>();
    }

    public int UserId { get; set; }

    public string PlayListName { get; set; }

    public string ImageUrl { get; set; }

    public IList<AudioTrack> AudioTracks { get; set; }
  }
}
