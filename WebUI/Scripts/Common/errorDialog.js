﻿/*
errorDialog = {
  showDialog: function (message, title) {
    if (title == null) title = 'Alert';
    errorDialog._show(title, message);
  },

  closeDialog: function () {
    if (errorDialog._isShown) {
      errorDialog._hide();
    }
  },

  _isShown: false,

  _show: function (title, msg) {
    errorDialog._handleOverlay('show');

    $('#dialog-container').removeClass('display-none');

    $("#dialog_title").text(title);
    $("#dialog_message").html(msg);
    $("#dialog_button").width(70);
    $("#dialog_button").one('click', function () {
      errorDialog._hide();
    });

    errorDialog._isShown = true;

    setTimeout(function () { errorDialog._updatePosition(); }, 100);
  },

  _hide: function () {
    $('#dialog-container').addClass('display-none');
    errorDialog._handleOverlay('hide');
    errorDialog._isShown = false;
  },

  _handleOverlay: function (status) {
    switch (status) {
      case 'show':
        $('#dialog_overlay').removeClass('display-none');
        $('#dialog_overlay').css("height", $(document).height());
        break;
      case 'hide':
        $('#dialog_overlay').addClass('display-none');
        break;
    }
  },

  _updatePosition: function () {
    var dialog = $("#dialog-container");
    var windowWidth = $(window).width();
    var windowHeight = $(window).height();

    var dialogWidth = dialog.width();
    var dialogHeight = dialog.height();

    var left = windowWidth - dialogWidth <= 0 ? 0 : (windowWidth - dialogWidth) / 2;
    var top = windowHeight - dialogHeight <= 0 ? 0 : (windowHeight - dialogHeight) / 2;

    dialog.css({ top: top, left: left });
  }
};

function showError(errorMessage) {
  //$('html, body').animate({ scrollTop: 0 }, 'fast');
  errorDialog.showDialog(errorMessage, "ОШИБКА");
}

function showWarning(warningMessage) {
  //$('html, body').animate({ scrollTop: 0 }, 'fast');
  errorDialog.showDialog(warningMessage, "ВНИМАНИЕ");
}

function closeError() {
  errorDialog.closeDialog();
}

//Mousetrap.bind('esc', function () { closeError(); closeConfirm(); });
//Mousetrap.bind('enter', function () { applyConfirm(); });

*/