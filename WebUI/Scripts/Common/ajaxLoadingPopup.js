﻿var loadingController = function loadingController() {
  var timeout = null;

  //Number of milliseconds to wait before ajax animation starts.
  var delayBy = 200;

  var pub = {};

  var actualAnimationStart = function actualAnimationStart() {
    $('#loading_container').removeClass('dialog-master-display-none');
    $('#loading_overlay').removeClass('dialog-master-display-none');
    $('#loading_overlay').css("height", $(document).height());
  };

  var actualAnimationStop = function actualAnimationStop() {
    $('#loading_container').addClass('dialog-master-display-none');
    $('#loading_overlay').addClass('dialog-master-display-none');
  };

  pub.startAnimation = function () {
    timeout = setTimeout(actualAnimationStart, delayBy);
  };

  pub.stopAnimation = function animationController$stopAnimation() {
    //If ajax call finishes before the timeout occurs, we wouldn't have 
    //shown any animation.
    clearTimeout(timeout);
    actualAnimationStop();
  }

  return pub;
}();

$(document).ready(function () {
  $(document).ajaxStart(loadingController.startAnimation);
  $(document).ajaxStop(loadingController.stopAnimation);
});
