﻿function initVplaylisteHelpers(vplayliste) {

  vplayliste.redirectToPage = function (url) {
    $(location).attr('href', url);
    $('html, body').animate({ scrollTop: 0 }, 'fast');
  }

  vplayliste.buildJPlaylist = function (tracks) {
    if (tracks == undefined || tracks.length < 1) {
      return [];
    }

    var result = [];

    var isKoModel = 'Id' in tracks[0] && typeof tracks[0].Id == 'function' ? true : false;
    for (var i = 0; i < tracks.length; i++) {
      var jPlayerTrack = {};

      if (isKoModel) {
        jPlayerTrack = {
          author: tracks[i].Artist(),
          title: tracks[i].Title(),
          mp3: tracks[i].Url(),
          discription: "",
          vkId: tracks[i].VkTrackId(),
          uid: tracks[i].OwnerId() + '_' + tracks[i].VkTrackId()
        };
      } else {
        jPlayerTrack = {
          author: tracks[i].artist,
          title: tracks[i].title,
          mp3: tracks[i].url,
          discription: "",
          vkId: tracks[i].aid,
          uid: tracks[i].owner_id + '_' + tracks[i].aid
        };
      }
      result.push(jPlayerTrack);
    }

    return result;
  };

  vplayliste.findIndexByPropNameValue = function (array, propName, value) {
    for (var i = 0; i < array.length; i++) {
      if (propName in array[i] && array[i][propName] == value) {
        return i;
      }
    }
  };

  vplayliste.shuffle = function (array) {
    var counter = array.length, temp, index;

    while (counter > 0) {
      index = Math.floor(Math.random() * counter);
      counter--;

      temp = array[counter];
      array[counter] = array[index];
      array[index] = temp;
    }

    return array;
  };

  vplayliste.testImage = function (url, validationResult, callback, timeout) {
    timeout = timeout || 5000;
    var timedOut = false, timer;
    var img = new Image();
    img.onerror = img.onabort = function () {
      if (!timedOut) {
        clearTimeout(timer);

        validationResult.valid = false;
        validationResult.error += "\nНекорректное изображение;";
        callback(validationResult);
      }
    };

    img.onload = function () {
      if (!timedOut) {
        clearTimeout(timer);
        callback(validationResult)
      }
    };

    img.src = url;
    timer = setTimeout(function () {
      timedOut = true;

      validationResult.valid = false;
      validationResult.error = "\nНекорректное изображение;";
      callback(validationResult);
    }, timeout);
  };

  vplayliste.validateAddEditPlaylistForm = function (imageUrl, name, callback) {
    var validationResult = {
      valid: true,
      error: ''
    };

    if (vplayliste.vkPlaylist.errors().length > 0 || name.length < 1 || name.length > 20) {
      validationResult.valid = false;
      validationResult.error += "Некорректное название; ";
    }

    if (imageUrl.length > 0) {
      var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?(.(jpg|bmp|png|gif|jpeg))/gi;
      var regex = new RegExp(expression);

      var result = imageUrl.match(regex);

      if (result == null) {
        validationResult.valid = false;
        validationResult.error += "\nНекорректный адрес изображения! Адрес должен заканчиваться на .jpg|.jpeg|.png|.gif; ";
        callback(validationResult);

        return;
      } else {
        vplayliste.testImage(imageUrl, validationResult, callback, 1000);
      }
    }
    else {
      callback(validationResult);
    }
  };

  vplayliste.ajax = function (url, request, isGet) {
    var promise;
    var requestType = "POST";

    if (isGet) {
      requestType = "GET"
    }

    promise = (function (url, request) {

      var dfd;
      dfd = new $.Deferred();

      $.ajax({
        type: requestType,
        url: url,
        contentType: "application/json; charset=utf-8",
        data: request,
        success: function (data) {
          if ('Error' in data) {
            showDialogMasterError(data.Error);
            dfd.reject();
          } else {
            dfd.resolve(data);
          }
        },
        error: function () {
          dfd.reject();
          showDialogMasterError('ошибка при запросе к серверу...');
        }
      });

      return dfd.promise();
    })(url, request);

    return promise;
  };
}