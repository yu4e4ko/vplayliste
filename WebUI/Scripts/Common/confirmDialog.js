﻿/*
confirmDialog = {
  showDialog: function (message, options) {
    return confirmDialog._show(message, options);
  },

  cancel: function(){
    if (confirmDialog._isShown) {
      $("#confirm-dialog_button_cancel").click();
    }
  },

  apply: function () {
    if (confirmDialog._isShown) {
      $("#confirm-dialog_button_ok").click();
    }
  },

  _isShown: false,

  _show: function (msg, options) {
    confirmDialog._isShown = true;

    var deferred = new $.Deferred();

    var title = "Confirm";
    if (typeof (msg) != "string") {
      title = msg.title;
      msg = msg.message;
    }

    confirmDialog._handleOverlay('show');

    $('#confirm-dialog-container').removeClass('display-none');

    $("#confirm-dialog_title").text(title);
    $("#confirm-dialog_message").html(msg);
    $("#confirm-dialog_button").width(70);
 
    var imageContainer = $("#confirm-dialog_image-container");
    imageContainer.hide();

    var input = $("#confirm-dialog_textinput");

    if (options.withInput) {
      input.show();
      var text = options.inputText || "";
      input.val(text);
    } else {
      input.hide();
    }

    if (options.imageSrc) {
      imageContainer.show();
      $("#confirm-dialog_image").attr("src", options.imageSrc);
    }

    $("#confirm-dialog_button_ok").one('click', function () {
      confirmDialog._hide();
      deferred.resolve();
    });

    $("#confirm-dialog_button_cancel").one('click', function () {
      confirmDialog._hide();
      deferred.reject();
    });

    return deferred.promise();
  },

  _hide: function () {
    $('#confirm-dialog-container').addClass('display-none');
    confirmDialog._handleOverlay('hide');
    confirmDialog._isShown = false;
  },

  _handleOverlay: function (status) {
    switch (status) {
      case 'show':
        $('#confirm-dialog_overlay').removeClass('display-none');
        $('#confirm-dialog_overlay').css("height", $(document).height());
        break;
      case 'hide':
        $('#confirm-dialog_overlay').addClass('display-none');
        break;
    }
  }
};

function showConfirm(question, callback, options) {
  options = options || {};

  confirmDialog.showDialog(question, options).done(function () {
    var text;
    if (options.withInput) {
      text = $("#confirm-dialog_textinput").val();
    }

    if (callback) {
      var data = options.data;

      if (data) {
        callback(data, text);
      } else {
        callback(text);
      }
    }
  });
}

function closeConfirm() {
  confirmDialog.cancel();
}

function applyConfirm() {
  confirmDialog.apply();
}
*/