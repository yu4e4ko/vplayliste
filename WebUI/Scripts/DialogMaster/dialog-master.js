﻿dialogMaster = {
  /*
  config.body = "error...."
  config.title = "title..."
  config.okCallback = function() {}
  config.cancelCallback = function() {}
  config.data = {}
  config.hideCancel = true
  */
  showDialog: function (config) {
    if (dialogMaster._isShown) {
      dialogMaster._messagePool.push(config);
      return;
    }

    dialogMaster._show(config);
  },
  
  clickOk: function(){
    $("#dialog-master-button-ok")[0].click();
  },

  clickCancel: function () {
    if (dialogMaster._withCancel) {
      $("#dialog-master-button-cancel")[0].click();
    } else {
      dialogMaster.clickOk();
    }
  },

  _data: {},

  _messagePool: [],

  _isShown: false,

  _withCancel: false,

  _show: function (config) {
    dialogMaster._isShown = true;
    dialogMaster._handleOverlay('show');

    $('#dialog-master-container').removeClass('dialog-master-display-none');

    dialogMaster._data = config.data;

    dialogMaster._buildTitle(config.title);
    dialogMaster._buildDialogBody(config.body);

    dialogMaster._bindOkButton(config.okCallback);

    if (config.hideCancel) {
      dialogMaster._withCancel = false;
      $("#dialog-master-button-cancel").hide();
    }
    else {
      $("#dialog-master-button-cancel").show();
      dialogMaster._withCancel = true;
      dialogMaster._bindCancelButton(config.cancelCallback);
    }

    setTimeout(function () { dialogMaster._updatePosition(); }, 100);
  },

  _hide: function () {
    $('#dialog-master-container').addClass('dialog-master-display-none');
    dialogMaster._handleOverlay('hide');
    dialogMaster._isShown = false;

    if (dialogMaster._messagePool.length > 0) {
      var config = dialogMaster._messagePool.splice(0, 1)[0];
      dialogMaster._show(config);
    }
  },

  _handleOverlay: function (status) {
    switch (status) {
      case 'show':
        $('#dialog-master-overlay').removeClass('dialog-master-display-none');
        $('#dialog-master-overlay').css("height", $(document).height());
        break;
      case 'hide':
        $('#dialog-master-overlay').addClass('dialog-master-display-none');
        break;
    }
  },

  _updatePosition: function () {
    var dialog = $("#dialog-master-container");
    var windowWidth = $(window).width();
    var windowHeight = $(window).height();

    var dialogWidth = dialog.width();
    var dialogHeight = dialog.height();

    var left = windowWidth - dialogWidth <= 0 ? 0 : (windowWidth - dialogWidth) / 2;
    var top = windowHeight - dialogHeight <= 0 ? 0 : (windowHeight - dialogHeight) / 2;

    dialog.css({ top: top, left: left });
  },

  _bindOkButton: function (callback) {
    $("#dialog-master-button-ok").off();
    $("#dialog-master-button-ok").on('click', function () {
      if (typeof callback === "function") {
        callback(dialogMaster._data);
      }

      dialogMaster._hide();
    }); 
  },

  _bindCancelButton: function (callback) {
    $("#dialog-master-button-cancel").off();
    $("#dialog-master-button-cancel").on('click', function () {
      if (typeof callback === "function") {
        callback(dialogMaster._data);
      }

      dialogMaster._hide();
    });
  },

  _buildDialogBody: function (body) {
    if (body) {
      $("#dialog-master-body").html(body);
    }
    else {
      $("#dialog-master-body").html('');
    }
  },

  _buildTitle: function (title) {
    if (title) {
      $("#dialog-master-title").text(title);
    }
  }
};

Mousetrap.bind('esc', function () { dialogMaster.clickCancel(); });
Mousetrap.bind('enter', function () { dialogMaster.clickOk(); });

function showDialogMasterError(errorMessage) {
  dialogMaster.showDialog({ title: "Ошибка", body: errorMessage, hideCancel: true });
}

function showDialogMasterWarning(warningMessage) {
  dialogMaster.showDialog({ title: "Внимание", body: warningMessage, hideCancel: true });
}