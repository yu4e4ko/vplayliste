﻿function VkSharing() {
  var self = this;

  self.sharedPlaylistId = ko.observable('0');

  self.wallPostUrl = ko.observable('');

  self.userId = function () {
    return vplayliste.vkUser.userId();
  };

  self.showSharedPlaylist = function () {

    var url = vkMusicUrls.getSharedPlaylistApiController + "/?id=" + self.sharedPlaylistId();
    vplayliste.ajax(url, {}, true).done(function (data) {
      if (data.PlayList != null) {
        vplayliste.vkPlaylist.currentPlaylist(ko.mapping.fromJS(data.PlayList));

        setTimeout(vplayliste.vkPlaylist.playCurrentPlayList, 500);
        vplayliste.redirectToPage(vkMusicUrls.sharedPage);
      } else {
        showDialogMasterError('Плейлист не найден. Возможно был удалел владельцем');
        vplayliste.redirectToPage(vkMusicUrls.search);
      }
    });
  };
  

  self.savePlayList = function () {
    if (vplayliste.vkPlaylist.playLists().length >= MaxPlaylistsCount) {
      showDialogMasterError("Максимум " + MaxPlaylistsCount + " плейлистов. Вы превышаете лимит");
      return;
    }

    var userId = self.userId();

    var url = vkMusicUrls.playListApiController;

    var tempName = vplayliste.vkPlaylist.currentPlaylist().Name()
      + '_shared_' + vplayliste.vkPlaylist.currentPlaylist().Id();

    var request = ko.toJSON({
      UserId: userId,
      PlayListName: tempName,
      ImageUrl: vplayliste.vkPlaylist.currentPlaylist().Image(),
      AudioTracks: vplayliste.vkPlaylist.currentPlaylist().AudioTracks()
    });
   
    vplayliste.ajax(url, request).done(function (data) {
      vplayliste.vkPlaylist.playLists.push(data);

      vplayliste.vkPlaylist.currentPlaylist({});
      vplayliste.redirectToPage(vkMusicUrls.playLists);
    });
  };


  self.getTracksFromPost = function () {
    var url = self.wallPostUrl();
    var wall = url.match(/wall[-]*[0-9]+_[0-9]+/);
    
    if (wall === null || wall === undefined) {
      showDialogMasterError('Некорректная ссылка');
      self.wallPostUrl('');
      vplayliste.vkAudio.typeOfSearch('');
      vplayliste.vkAudio.searchResult([]);
      return;
    }

    var postId = wall[0].split('wall')[1];
    vplayliste.vkAudio.typeOfSearch('');

    VK.Api.call('wall.getById', { posts: [postId] }, function (r) {
      if ('error' in r) {
        showDialogMasterError(r.error.error_msg);
        return;
      }

      var tracks = [];

      if (r.response && r.response.length > 0) {
        if ('attachments' in r.response[0]) {
          var attachments = r.response[0].attachments;

          for (var i = 0; i < attachments.length; i++) {
            if (attachments[i].type == "audio") {
              tracks.push(attachments[i].audio);
            }
          }
          
          vplayliste.vkAudio.searchResult(tracks);
          vplayliste.jPlayer.setPlaylist(tracks);
        }
      }
      
      if (tracks.length < 1) {
        showDialogMasterError('Аудиозаписи в данном посте не найдены');
        self.wallPostUrl('');
        vplayliste.vkAudio.typeOfSearch('');
        vplayliste.vkAudio.searchResult([]);
      } else {
        self.wallPostUrl('');
        vplayliste.vkAudio.typeOfSearch('Музыка из поста');
      }      
    });
  };
}