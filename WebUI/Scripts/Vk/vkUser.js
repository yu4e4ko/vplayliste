﻿function VkUser() {
  var self = this;

  self.data = {};
  
  self.api = "//vk.com/js/api/openapi.js";
  self.appID = 4944225;
  
  //self.appPermissions = "audio,photos";
  self.appPermissions = "8204";

  self.firstName = ko.observable('');
  self.lastName = ko.observable('');
  self.userId = ko.observable('');
  self.photo = ko.observable('');
  self.exportAccess = ko.observable(false);
  self.downloadAccess = ko.observable(false);
  
  self.isAuthorized = ko.observable(false);
  

  self.fullName = ko.computed(function () {
    return self.firstName() + " " + self.lastName();
  }, self);


  self.login = function() {
    VK.init({ apiId: self.appID });

    function authInfo(response) {
      if (response.session) {
        self.data.user = response.session.user;

        self.getMyInfo();
      } else {
        showDialogMasterError("Авторизоваться не удалось!");
      }
    }

    VK.Auth.login(authInfo, self.appPermissions);
  };
  

  self.logout = function () {
    VK.Auth.logout(function () {
      self.data = {};
      self.isAuthorized(false);
    }); 
  };


  self.getMyInfo = function() {
    VK.Api.call('users.get', { fields: ['photo_100', 'online'] }, function (r) {
      if (r.response) {
        r = r.response;
        
        self.firstName(r[0].first_name);
        self.lastName(r[0].last_name);
        self.userId(r[0].uid);
        self.photo(r[0].photo_100);

        self.isAuthorized(true);
      } else {
        showDialogMasterError("Не удалось получить личную информацию");
      }
    });
  };

  self.vkWidgetGroup = function() {
    VK.Widgets.Group("vk_groups", { mode: 0, width: "220", height: "400", color1: 'FFFFFF', color2: '016677', color3: '016677' }, 96587137);
  };
}