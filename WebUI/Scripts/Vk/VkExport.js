﻿function VkExport() {
  var self = this;

  var addTrack = function (albumId, track) {
    var deferred = new $.Deferred();

    var addRequest = {
      audio_id: track.VkTrackId(),
      owner_id: track.OwnerId(),
      album_id: albumId,
    };

    function callAudioAdd(request) {
      VK.Api.call('audio.add', request, function (addResponse) {
        if (addResponse.error) {
          if (addResponse.error.error_code == 14) {
            var captchaImage = addResponse.error.captcha_img;
            var captchaSid = addResponse.error.captcha_sid;

            var callBack = function (text) {
              request.captcha_sid = captchaSid;
              request.captcha_key = text;

              return callAudioAdd(request);
            };

            var confirmRequest = {
              title: "Captcha",
              okCallback: function () { callBack($("#captchaValue").val()) },
              body: '<div>'
                + '<img src=' + captchaImage + '>'
                + '<div><input type="text" id="captchaValue"/></div>'
                +'</div>'
            }

            dialogMaster.showDialog(confirmRequest);
          }
          else {
            console.log("REJECTED");
            console.dir(addResponse.error);
            deferred.reject();
          }
        } else {
          deferred.resolve();
        }
      });
    }

    callAudioAdd(addRequest);

    return deferred.promise();
  }

  var startExport = function (playlist) {
    loadingController.startAnimation();

    var tracks = ko.unwrap(playlist.AudioTracks);
    var name = ko.unwrap(playlist.Name);
    var vkAlbumName = name + "_vplayliste_(" + new Date().toLocaleString() + ")";

    VK.Api.call('audio.addAlbum', { title: vkAlbumName }, function (r) {
      if ('error' in r) {
        showDialogMasterError(r.error.error_msg);
        return;
      }

      if (!r.response || !r.response.album_id) {
        return;
      }

      var albumId = r.response.album_id;
      var addTrackPromise = addTrack(albumId, tracks[0]);

      var i = 1;
      (function () {
        if (i < tracks.length) {
          var track = tracks[i];

          function addTrackClosure(index) {
            addTrackPromise.always(function () {
              addTrackPromise = addTrack(albumId, tracks[index]);

              if (index == tracks.length - 1) {
                loadingController.stopAnimation();
                alert("Export complete");
              }
            });
          }

          addTrackClosure(i);
          i++;

          setTimeout(arguments.callee, 2500);
        }
      })();
    });
  }

  self.exportAudioInVk = function (playlist) {
    var request = {
      title: "Экспорт",
      body: "Экспортировать в ВК?",
      okCallback: startExport,
      data: playlist
    };

    dialogMaster.showDialog(request);
  }
}