﻿function VkPlayList() {
  var self = this;
  
  self.userId = function () {
    return vplayliste.vkUser.userId();
  };

  self.playLists = ko.observableArray([]);

  self.userGroups = ko.observableArray([]);
  
  self.newPlaylistName = ko.observable('').extend({ minLength: 2, maxLength: 20, required: true });
  
  self.newPlaylistImage = ko.observable('').extend({ maxLength: 150 });

  self.currentPlaylist = ko.observable({});

  self.currentPlaylistHasTracks = ko.computed(function() {
    return ('AudioTracks' in self.currentPlaylist() && self.currentPlaylist().AudioTracks().length > 0);
  }, self);


  self.getImageUrlOrDefault = function (url) {
    var baseUrl = vplaylisteLogo;
    
    if (url) {
      if (typeof url == 'function' && url().length > 5) {
        return url();
      }else if (url.length > 5) {
        return url;
      } else {
        return baseUrl;
      }
    }

    return baseUrl;
  };


  self.setUserPlaylists = function () {
    var userId = self.userId();
    
    if (userId) {
           
      var url = vkMusicUrls.playListApiController + "/?userId=" + userId;
      vplayliste.ajax(url, {}, true).done(function (data) {
        self.playLists(data.PlayLists);
      });
    }
  };


  self.updateTrack = function (track) {
    var artist = track.Artist();
    var title = track.Title();

    var songName = artist + " - " + title;

    VK.Api.call('audio.search', { q: [songName], auto_complete: ['1'], count: [100] }, function (r) {
      if ('error' in r) {
        showDialogMasterError(r.error.error_msg);
        return;
      } else if (r.response.length < 2) {
        showDialogMasterError('Ничего не найдено');
        return;
      }

      var searchResult = r.response;
      var resultTrack = {};

      for (var i = 0; i < searchResult.length; i++) {
        if (searchResult[i].artist
          && searchResult[i].title
          && searchResult[i].artist.trim().toLowerCase() == artist.trim().toLowerCase()
          && searchResult[i].title.trim().toLowerCase() == title.trim().toLowerCase()) {
          resultTrack = searchResult[i];
          break;
        }
      }

      if (!resultTrack.aid || resultTrack.aid < 0) {
        showDialogMasterError('Ничего не найдено');
        return;
      }

      self._replaceUpdatedTrack(track, resultTrack);
    });  
  }


  self._replaceUpdatedTrack = function (track, vkTrack) {
    var playlistId = self.currentPlaylist().Id();
    var tracks = self.currentPlaylist().AudioTracks();

    for (var i = 0; i < tracks.length; i++) {
      if (tracks[i].Id() == track.Id()) {
        var jPlaylist = vplayliste.jPlayer.mainPlayer.playlist;

        if (jPlaylist[i].vkId != tracks[i].VkTrackId()) {
          continue;
        }

        tracks[i].Artist(vkTrack.artist);
        tracks[i].Title(vkTrack.title);
        tracks[i].VkTrackId(vkTrack.aid);
        tracks[i].Url(vkTrack.url);
        tracks[i].OwnerId(vkTrack.owner_id);
        
        var request = ko.toJSON({
          PlayListId: playlistId,
          VkTrackId: vkTrack.aid,
          Artist: vkTrack.artist,
          Title: vkTrack.title,
          Url: vkTrack.url,
          OwnerId: vkTrack.owner_id,
          TrackId: track.Id()
        });

        vplayliste.ajax(vkMusicUrls.updateTrackController, request).done(function (response) {
          $('[name=track_' + tracks[i].VkTrackId() + ']').removeClass('track-deny');
          $('[name=track_' + tracks[i].VkTrackId() + '_update]').addClass("display-none");

          jPlaylist[i].mp3 = vkTrack.url;

          vplayliste.jPlayer.setPlaylist(tracks);
        });
     
        break;
      }
    }
  }


  self.viewPlayList = function (playList) {
    if ("Id" in self.currentPlaylist() && self.currentPlaylist().Id() == playList.Id && vplayliste.jPlayer.playlist.length == self.currentPlaylist().AudioTracks().length) {
      vplayliste.redirectToPage(vkMusicUrls.viewPlayListInfo);
    }
    else {
      self.getUserGroups();
      self.currentPlaylist(ko.mapping.fromJS(playList));

      vplayliste.redirectToPage(vkMusicUrls.viewPlayListInfo);
      self.playCurrentPlayList();
    }

    vplayliste.vkAudio.searchResult([]);
    vplayliste.vkAudio.typeOfSearch('');
    vplayliste.vkAudio.searchText('');

    document.title = playList.Name;
  };


  self.getUserGroups = function() {
    if (self.userGroups().length != 0) {
      return;
    }
    
    var user_id = self.userId();
    
    VK.Api.call('groups.get', { user_id: [user_id], filter: ["admin", "editor", "moder"], extended: ["1"] }, function (r) {
      if ('error' in r) {
        showDialogMasterError(r.error.error_msg);
      }

      var response = r.response;
      
      if (response.length < 2) {
        self.userGroups([false]);
        return;
      }

      var groups = [];

      for (var i = 1; i < response.length; i++) {
        if ("gid" in response[i]) {
          groups.push({id: response[i].gid, name: response[i].name });
        }
      }

      self.userGroups(groups);
    });
  };


  self.getAudioTracksCountInCurrentPlaylist = ko.computed(function() {
    if ("AudioTracks" in self.currentPlaylist()) {
      return self.currentPlaylist().AudioTracks().length;
    }
    return 0;
  }, self);
  

  self.playCurrentPlayList = function () {
    var audioTracks = self.currentPlaylist().AudioTracks();

    vplayliste.jPlayer.setPlaylist(audioTracks);
  };


  self._getTracksForPost = function () {
    var attachments = [];
    var attachmentItem = '';

    var audioTracks = self.currentPlaylist().AudioTracks();

    for (var i = 0; i < audioTracks.length; i++) {
      attachmentItem = 'audio' + audioTracks[i].OwnerId() + '_' + audioTracks[i].VkTrackId();
      attachments.push(attachmentItem);

      if (i == 9) {
        break;
      }
    }

    return attachments;
  }

  self._uploadImageAndWallPost = function (owner_id, image) {
    var getRequest = {};

    if (owner_id < 0) {
      getRequest.uid = self.userId();
      getRequest.gid = owner_id * -1;
    } else {
      getRequest.uid = owner_id;
    }

    VK.api('photos.getWallUploadServer', getRequest, function (data) {
      if (data.response) {
          
        var uploadRequest = {
          User: owner_id,
          Action: 'upload',
          UploadUrl: data.response.upload_url,
          ImagePath: image
        }       

        $.post(vkMusicUrls.uploadVkPhotoController, uploadRequest, function (json) {
          if ("Error" in json) {
            showDialogMasterError(json.Error);
            return;
          }

          var saveRequest = {
            server: json.server,
            photo: json.photo,
            hash: json.hash
          }

          // if group
          if (owner_id < 0) {
            saveRequest.uid = self.userId();
            saveRequest.gid = owner_id * -1;
          } else {
            saveRequest.uid = owner_id;
          }

          VK.api("photos.saveWallPhoto", saveRequest, function (data) {
            var attachments = [];

            var playlistId = self.currentPlaylist().Id();
            var url = window.location.origin + '/VkMusic/Shared/?id=' + playlistId;

            attachments.push(data.response['0'].id + ',' + url);

            var message = "&#9654; плейлист: " + self.currentPlaylist().Name();
            message += '\n&#10145; ' + url;

            var tracks = self._getTracksForPost();
            attachments.push.apply(attachments, tracks);

            VK.Api.call('wall.post', { owner_id: [owner_id], attachments: [attachments], message: [message] }, function (r) {
              if ('error' in r) {
                showDialogMasterError(r.error.error_msg);
              }
            });
          });
        }, 'json');
      }
    });
  }


  self._wallPostWithDefaultImage = function (owner_id) {
    var attachments = [];

    var playlistId = self.currentPlaylist().Id();
    var url = window.location.origin + '/VkMusic/Shared/?id=' + playlistId;

    attachments.push('photo-96587137_385693488,' + url);

    var message = "&#9654; плейлист: " + self.currentPlaylist().Name();
    message += '\n&#10145; ' + url;

    var tracks = self._getTracksForPost();
    attachments.push.apply(attachments, tracks);

    VK.Api.call('wall.post', { owner_id: [owner_id], attachments: [attachments], message: [message] }, function (r) {
      if ('error' in r) {
        showDialogMasterError(r.error.error_msg);
      }
    });
  }


  self.postPlayListOnWall = function (groupId) {
    if (groupId === undefined) {
      showDialogMasterWarning("Вы не являетесь администратором ни одной из групп.");
      return;
    }

    var owner_id = 0;
    
    if (isNaN(groupId) == false) {
      owner_id = 0 - groupId;
    } else {
      owner_id = self.userId();
    }
    
    var image = self.currentPlaylist().Image();

    if (image && image.length > 0) {
      self._uploadImageAndWallPost(owner_id, image);    
    }
    else {
      self._wallPostWithDefaultImage(owner_id);
    }
  };


  self.removePlayList = function (playlist) {
    var request = {
      title: "Удалeние плейлиста",
      body: "Удалить плейлист?",
      okCallback: self.deletePlayList,
      data: playlist
    };

    dialogMaster.showDialog(request);
  };


  self.deletePlayList = function (playlist) {
    if (playlist === undefined || playlist.Id < 0) {
      return;
    }

    var request = JSON.stringify({
      id: playlist.Id
    });

    vplayliste.ajax(vkMusicUrls.deletePlayListApiController, request).done(function (data) {
      self.playLists.remove(playlist);
    });
  };


  self.redirectToEditPlaylistPage = function (playList) {
    if ("Id" in playList) {
      self.currentPlaylist(ko.mapping.fromJS(playList));
      self.newPlaylistImage(playList.Image);
      self.newPlaylistName(playList.Name);

      vplayliste.redirectToPage(vkMusicUrls.editPlayList);
    } else {
      
      self.currentPlaylist({});
      self.newPlaylistImage('');
      self.newPlaylistName('');
      vplayliste.redirectToPage(vkMusicUrls.editPlayList);
    }
  };


  self.submitAddEditPlaylist = function (validationResult) {
    if (validationResult.valid == false) {
      showDialogMasterError(validationResult.error);
      return;
    }

    var userId = self.userId();
    var newName = self.newPlaylistName();

    var url = '';
    var request = {};
    var editMode = false;
    
    if ("Id" in self.currentPlaylist()) {
      editMode = true;
      url = vkMusicUrls.editPlayListController;
      
      request = ko.toJSON({
        PlaylistId: self.currentPlaylist().Id(),
        PlayListName: newName,
        ImageUrl: self.newPlaylistImage()
      });

    } else {
      
      if (self.playLists().length >= MaxPlaylistsCount) {
        showDialogMasterError("Максимум " + MaxPlaylistsCount + " плейлистов. Вы превышаете лимит");
        return;
      }

      url = vkMusicUrls.playListApiController;
      
      request = ko.toJSON({
        UserId: userId,
        PlayListName: newName,
        ImageUrl: self.newPlaylistImage()
      });
    }

    vplayliste.ajax (url, request).done(function (data) {
            if (editMode) {
              var index = vplayliste.findIndexByPropNameValue(self.playLists(), "Id", self.currentPlaylist().Id());
              self.playLists.replace(self.playLists()[index], data);      
            } else {          
              self.playLists.push(data);       
            }

            self.newPlaylistImage('');
            self.newPlaylistName('');
            self.currentPlaylist({});
            vplayliste.redirectToPage(vkMusicUrls.playLists);
    });
  }

  
  self.addEditPlayList = function () {
    var newName = self.newPlaylistName();
    var imageUrl = self.newPlaylistImage();

    vplayliste.validateAddEditPlaylistForm(imageUrl, newName, self.submitAddEditPlaylist);
  };


  self.currentPlaylistLink = function () {
    var playlistId = self.currentPlaylist().Id();
    var url = window.location.origin + '/VkMusic/Shared/?id=' + playlistId;
    return url;
  };

  
  self.exportInVk = function () {
    vplayliste.vkExport.exportAudioInVk(ko.unwrap(self.currentPlaylist()))
  };
}