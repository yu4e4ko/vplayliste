﻿function VkAudio () {
  var self = this;

  self.userId = function() {
    return vplayliste.vkUser.userId();
  };
  
  self.searchText = ko.observable('').extend({ minLength: 2, maxLength: 60 });

  // search by text or my music list from vk.com
  self.typeOfSearch = ko.observable('');
  
  self.searchResult = ko.observableArray([]);


  self.getUserAudioList = function() {
    VK.Api.call('audio.get', {}, function (r) {
      if ('error' in r) {
        showDialogMasterError(r.error.error_msg);
        return;
      }

      self.searchText('');
      self.typeOfSearch('Моя музыка');
      self.showSearchResultAndPlay(r);
    });
  };
  

  self.searchAudio = function () {
    if (self.errors().length > 0 || self.searchText().length < 1) {
      return;
    }
    
    VK.Api.call('audio.search', { q: [self.searchText()], auto_complete: ['1'], count: [100] }, function (r) {
      self.typeOfSearch('');
      
      if ('error' in r) {
        showDialogMasterError(r.error.error_msg);
        return;
      } else if (r.response.length < 2) {
        showDialogMasterError('Ничего не найдено');
        return;
      }
      
      self.typeOfSearch('Поиск: ' + self.searchText());
      self.searchText('');
      self.showSearchResultAndPlay(r);
    });
  };


  self.showSearchResultAndPlay = function(response) {
    self.searchResult([]);
    var result = response.response;

    if (result.length > 0 && typeof result[0] == 'number') {
      result.shift();
    }

    if (result.length > 500) {
      result = result.splice(0, 500);

      showDialogMasterWarning("Отображены только первые 500 треков");
    }
      
    self.searchResult(result);

    vplayliste.jPlayer.setPlaylist(result, true);
  };


  self.redirectAndCleanSearchResult = function() {
    vplayliste.redirectToPage(vkMusicUrls.search);
  };

  self.addToPlaylist = function (track, playList) {
    if (playList.Id == 0) {
      return;
    }

    if (playList.AudioTracks && playList.AudioTracks.length >= MaxTracksCount) {
      showDialogMasterError("Максимум " + MaxTracksCount + " треков в плейлисте. Вы превышаете лимит");
      return;
    }

    var request = {};
    var trackId = -1;
    if ('Id' in track && typeof track.Id == 'function') {
      trackId = track.VkTrackId();
      request = ko.toJSON({
        PlayListId: playList.Id,
        VkTrackId: trackId,
        Artist: track.Artist(),
        Title: track.Title(),
        Url: track.Url(),
        OwnerId: track.OwnerId()
      });
    } else {
      trackId = track.aid;
      request = ko.toJSON({
        PlayListId: playList.Id,
        VkTrackId: trackId,
        Artist: track.artist,
        Title: track.title,
        Url: track.url,
        OwnerId: track.owner_id
      });
    }

    for (var i = 0; i < playList.AudioTracks.length; i++) {
      if (playList.AudioTracks[i].VkTrackId == trackId) {
        showDialogMasterError("Трек уже добавлен в плейлист");
        return;
      }
    }

    vplayliste.ajax(vkMusicUrls.addAudioTrackController, request).done(function (data) {
      var index = vplayliste.vkPlaylist.playLists().indexOf(playList);
      vplayliste.vkPlaylist.playLists.replace(vplayliste.vkPlaylist.playLists()[index], data);
    });
  };


  self.removeAudioTrack = function (track) {
    if (track === undefined || track.Id() < 0) {
      return;
    }

    var request = JSON.stringify({
      id: track.Id()
    });

    vplayliste.ajax(vkMusicUrls.deleteAudioTrackApiController, request).done(function (data) {
      var currentPlaylist = vplayliste.vkPlaylist.currentPlaylist();

      currentPlaylist.AudioTracks.remove(track);
      currentPlaylist.AudioTracks(vplayliste.vkPlaylist.currentPlaylist().AudioTracks());

      for (var i = 0; i < vplayliste.vkPlaylist.playLists().length; i++) {
        if (vplayliste.vkPlaylist.playLists()[i].Id == currentPlaylist.Id()) {
          vplayliste.vkPlaylist.playLists()[i].AudioTracks = currentPlaylist.AudioTracks()
        }
      }

      var mainPlayer = vplayliste.jPlayer.mainPlayer;
      var playList = mainPlayer.playlist;

      for (var i = 0; i < playList.length; i++) {
        if (track.VkTrackId() == playList[i].vkId) {
          var index = mainPlayer.original.indexOf(playList[i]);

          mainPlayer.original.splice(index, 1);
          mainPlayer.playlist.splice(index, 1);

          if (index == mainPlayer.current && mainPlayer.current != 0) {
            mainPlayer.current -= 1;
          }
          else {
            for (var i = 0; i < mainPlayer.original.length; i++) {
              if(mainPlayer._currentTrackId == mainPlayer.original[i].vkId) {
                mainPlayer.current = i;
                break;
              }
            }
          }

          mainPlayer._refresh();
          break;
        }
      }
    });
  };

  
  self.downloadTrack = function (index, track) {
    track = ko.mapping.toJS(track);
    var name = track.Artist + " - " + track.Title;
    var url = vplayliste.jPlayer.mainPlayer.playlist[index].mp3;
    
    var link = document.createElement('a');
    if (typeof link.download === 'string') {
      link.href = url;
      link.download = name;
      link.name = name;
      link.target = '_blank';
      link.type = "file"

      //Firefox requires the link to be in the body
      document.body.appendChild(link);

      //simulate click
      link.click();

      //remove the link when done
      document.body.removeChild(link);
    } else {
      window.open(url);
    }
  }
}