﻿var vplayliste;

function InitKOModels() {
  var self = this;

  self.vkUser = new VkUser();
  self.vkAudio = new VkAudio();
  self.vkPlaylist = new VkPlayList();
  self.vkSharing = new VkSharing();
  self.vkExport = new VkExport();

  self.jPlayer = {};

  self.initAudioPlayer = function() {
    self.jPlayer = new JPlayer();
    self.jPlayer.init();
  };

  self.loadUserData = function() {
    self.vkUser.vkWidgetGroup();
    if (sharedId > 0) {
      self.vkPlaylist.setUserPlaylists();
      self.initAudioPlayer();

      self.vkSharing.sharedPlaylistId(sharedId);
      self.vkSharing.showSharedPlaylist();

      sharedId = 0;
    }
    else {
      self.vkPlaylist.setUserPlaylists();
      self.initAudioPlayer();
      vplayliste.redirectToPage(vplayliste.hrefHash + 'start/home');
    }
  };

  ko.validation.init({
    registerExtenders: true,
    messagesOnModified: true,
    insertMessages: true,
    parseInputAttributes: true,
    messageTemplate: null
  }, true);
  
  self.vkAudio.errors = ko.validation.group(self.vkAudio);
  self.vkPlaylist.errors = ko.validation.group(self.vkPlaylist);
}


$(function () {
  jQuery.browser = {};

  //Note: don't touch!
  (function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
      jQuery.browser.msie = true;
      jQuery.browser.version = RegExp.$1;
    }
  })();

  // init vplayliste
  vplayliste = new InitKOModels();
  vplayliste.hrefHash = '#!/';
  initVplaylisteHelpers(vplayliste);
  var container = document.getElementById('mainContainer');

  pager.Href.hash = vplayliste.hrefHash;
  pager.extendWithPage(vplayliste);
  ko.applyBindings(vplayliste, container);
  pager.start();

  console.log("ko and pager inited");

  vplayliste.vkUser.login();
});