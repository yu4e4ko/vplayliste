﻿function JPlayer() {
  var self = this;

  self.cssSelector = {
    jPlayer: "#jquery_jplayer_1",
    cssSelectorAncestor: "#jp_container_1"
  };

  self.mainPlayer = {};

  self.playlist = [];

  self.currentIndex = ko.observable('');

  self.playingNow = ko.observable('');

  self.activeTrack = ko.computed(function() {
    if (self.playingNow() === true) {
      return self.currentIndex();
    }
    return -1;
  },self);

  self.options = {
    swfPath: "js",
    supplied: "oga, mp3",
    wmode: "window",
    smoothPlayBar: false,
    keyEnabled: true
  };
  

  self.init = function() {
    self.mainPlayer = new jPlayerPlaylist(self.cssSelector, self.playlist, self.options);
    self.mainPlayer._init();
  };

  self.playSelectedTrack = function (index, track) {
    var info = '';

    if (typeof track.Artist == "function") {
      info = ('<b>' + track.Artist() + '</b> - ' + track.Title()).substr(0, 50);
    } else {
      info = ('<b>' + track.artist + '</b> - ' + track.title).substr(0, 50);
    }

    $('#name-of-the-song-that-plays').html(info);

    if (self.mainPlayer.current == index) {
      self.mainPlayer.play();
      return;
    }

    self.mainPlayer.select(index, true);
    self.mainPlayer.play(); 
  };

  self.play = function() {
    self.mainPlayer.play();
  };
  
  self.prev = function () {
    self.mainPlayer.previous();
  };
  
  self.pause = function () {
    self.mainPlayer.pause();
  };
  
  self.next = function () {
    self.mainPlayer.next();
  };

  self.setPlaylist = function (tracks, isNotUserPlaylist) {
    var jPlaylist = vplayliste.buildJPlaylist(tracks);

    if (jPlaylist.length < 1) {
      return;
    }

    var uids = [];

    for (var i = 0; i < jPlaylist.length; i++) {
      uids.push(jPlaylist[i].uid);
    }

    VK.Api.call('audio.getById', { audios: [uids] }, function (r) {
      if ('error' in r) {
        showDialogMasterError(r.error.error_msg);
        return;
      }

      var response = r.response;
      var notLoaded = 'Нет доступа к следующим трекам:</br>';
      var notLoadedExists = false;
      var audioTracksExists = false;

      var audioTracks = [];

      if ("AudioTracks" in vplayliste.vkPlaylist.currentPlaylist()) {
        audioTracksExists = true;
        audioTracks = vplayliste.vkPlaylist.currentPlaylist().AudioTracks();
      }

      for (var i = j = 0; i < response.length; i++, j++) {
        if (response[i].aid == jPlaylist[j].vkId) {
          jPlaylist[j].mp3 = response[i].url;

          if (!isNotUserPlaylist && audioTracksExists) {
            audioTracks[j].Url(response[i].url);
          }
        } else {
          notLoaded += ' - ' + jPlaylist[j].author + ' - ' + jPlaylist[j].title + ';</br>';

          $('[name=track_' + jPlaylist[j].vkId + ']').addClass('track-deny');
          $('[name=track_' + jPlaylist[j].vkId + '_update]').removeClass("display-none");

          i--;
          notLoadedExists = true;
        }
      }

      if (!isNotUserPlaylist) {
        vplayliste.vkPlaylist.currentPlaylist().AudioTracks(audioTracks);
      }

      if (notLoadedExists) {
        showDialogMasterWarning(notLoaded);
      }

      setTimeout(self.mainPlayer.setPlaylist(jPlaylist), 200);
    });
  };
}